# Tutorials

These files house the lazy man's approach to programming. It comprises notes made by the author in the course of leaning a particular language for the first time.